import React from "react";
import { Link } from "react-router-dom";
import MainLayout from "../layouts/MainLayout";

class Detail extends React.Component {
    state = {
        skills: [
            {
                id: 1,
                name: "Javascript",
                desc: "Javascript Description",
                image: require("../images/js.png")
            },
            {
                id: 2,
                name: "React",
                desc: "React Description",
                image: require("../images/react.png")
            },
            {
                id: 3,
                name: "VueJs",
                desc: "VueJs Description",
                image: require("../images/vue.png")
            },
            {
                id: 4,
                name: "Svelt",
                desc: "Svelt Description",
                image: require("../images/svelt.png")
            }
        ],
        data: ""
    }

    componentDidMount() {
        const id = this.props.match.params.id
        const data = this.state.skills.find(item => item.id === parseInt(id))
        this.setState({ data: data })
    }

    render() {
        const { data } = this.state
        return (
            <MainLayout>
                <div style={container}>
                    <h1>{data.name}</h1>
                    <div style={box}>
                        <img style={img} src={data.image} alt={data.name} />
                        <div>
                            <p>{data.desc}</p>
                            <Link to="/">back home</Link>
                        </div>
                    </div>
                </div>
            </MainLayout>
        )
    }
}

export default Detail;

const container = {
    padding: "0 6rem",
}

const img = {
    width: "15rem",
    height: "20rem",
    marginRight: "2rem"
}

const box = {
    display: "flex",
}